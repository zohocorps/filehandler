import java.io.File;
import java.io.IOException;

public class DirectoryHandle {
    public static String changedirectory(String path, String command){

        if(command.equals("cd ..")){
            if (path.contains("\\"))
                path=path.substring(0,path.lastIndexOf("\\"));
        }
        else if(command.replace("cd","").trim().equals("~")){
            path=FileMain.initialpath;
        }
        else if(!command.replace(".","").trim().equals("cd")){
            String com = path+"\\"+command.substring(3).trim();
            File fp = new File(com);
            if(fp.exists())
                path=com;
            else
                System.out.println("The system cannot find the path specified.");
        }
        else
            System.out.println("The system cannot find the path specified.");
        return path;
    }
    public static void createdirectory(String path,String command) {
        File directory = new File(path+"\\"+command.substring(6).trim());
        if (directory.mkdir()) {
            System.out.println("Directory created successfully");
        } else {
            System.out.println("Failed to create directory");
        }
    }
    public static void removedir(String path,String command) throws IOException {
        File directory = new File(path+"\\"+command.substring(6).trim());
        if (directory.exists() && directory.isDirectory()){
            if (directory.delete())
                System.out.println("Directory deleted.");
            else
                System.out.println("You cannot delete a directory when there are files");
        }
        else {
            System.out.println("Directory not found.");
        }
    }
    public static void rename(String command, String path){
        String oldname = command.substring(3,command.lastIndexOf(" ")).trim();
        String newname = command.substring(command.lastIndexOf(" ")).trim();
        File oldFile = new File(path+"\\"+oldname);
        File newFile = new File(path+"\\"+newname);
        if (oldFile.renameTo(newFile)) {
            System.out.println("File renamed successfully");
        } else {
            System.out.println("Rename failed");
        }
    }
}
