import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class FileMain{
    static String initialpath = "C:\\Users\\naveen-pt6444";
    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        Scanner in = new Scanner(System.in);
        String command="empty";
        String path = "C:\\Users\\naveen-pt6444";

        while(!command.equals("exit")){
            System.out.print("\n"+path+">");
            command = in.nextLine().trim().toLowerCase();

            if(command.startsWith("cd")){
                path = DirectoryHandle.changedirectory(path,command);
            }
            else if(command.startsWith("ls")){
                Filehandle.listfiles(path,command);
            }
            else if(command.startsWith("mkdir")){
                DirectoryHandle.createdirectory(path,command);
            }
            else if(command.startsWith("touch")) {
                Filehandle.createfile(path,command);
            }
            else if(command.startsWith("rmdir")) {
                DirectoryHandle.removedir(path,command);
            }
            else if(command.startsWith("cat")){
                if(command.contains(">"))
                    Filehandle.writefile(path,command);

                else
                    Filehandle.readfile(path,command);
            }
            else if (command.startsWith("rm")){
               Filehandle.deletefile(path,command);
            }
            else if (command.startsWith("cp")) {
                Filehandle.copyfile(path,command);
            }
            else if (command.startsWith("mv")){
                DirectoryHandle.rename(command,path);
            }
            else{
                System.out.println(command+" is not recognized as an internal or external command,\n operable program or batch file.");
            }
        }
    }
}
