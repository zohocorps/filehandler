import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Filehandle {
    public static void listfiles(String path,String command) throws IOException {
        if(command.trim().equals("ls")){
            Files.list(Paths.get(path))
                    .forEach(paths -> System.out.println(paths.getFileName()));
        }
        else{
            String path1=path+"\\"+command.substring(3).trim();
            File f = new File(path1);
            if(f.isDirectory() && f.exists()){
                Files.list(Paths.get(path1))
                        .forEach(paths -> System.out.println(paths.getFileName()));
            }
            else{
                System.out.println("Wrong path specified");
            }
        }


    }
    public static void createfile(String path,String command) throws IOException {
        String command1="\\"+command.substring(6).trim();
        String command2 = command.substring(0,6).trim()+command1;
        File directory = new File(path+"\\"+command2.substring(6,command2.lastIndexOf("\\")).trim());
        if (!directory.exists())
            directory.mkdirs();

        File file = new File(directory,command.substring(command2.lastIndexOf("\\")).trim());

        if (file.createNewFile())
            System.out.println("File created: " + file.getName());
        else
            System.out.println("File already exists.");
    }
    private static void singlefile(File file) throws FileNotFoundException{
        if(file.exists()){
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                System.out.println(sc.nextLine());
            }
        }
        else{
            System.out.println("file not available");
        }
    }
    private static void folders(File file) throws ExecutionException, InterruptedException, FileNotFoundException {
        if(file.isDirectory()) {
            File listoffiles[] = file.listFiles();
            for (File f : listoffiles) {
                if (f.isFile()) {
                    Scanner in = new Scanner(f);
                    while (in.hasNextLine()) {
                        System.out.println(in.nextLine());
                    }
                }
            }
        }
    }
    public static void readfile(String path, String command) throws FileNotFoundException, ExecutionException, InterruptedException {
        File file = new File(path+"\\"+command.substring(4).trim());
        if(command.contains(".txt")){
           singlefile(file);
        }
        else {
            folders(file);
        }
    }
    public static void deletefile(String path,String command){
        if (command.contains(".txt")) {
            File file = new File(path + "\\" + command.substring(3));
            if (file.delete()) {
                System.out.println("File deleted successfully");
            } else {
                System.out.println("Failed to delete the file");
            }
        }
        else{
            System.out.println("No text file detected");
        }
    }
    public static void copyfile(String path, String command){
        String filename = command.substring(3,command.lastIndexOf(" ")).trim();
        String copypath = command.substring(command.lastIndexOf(" ")).trim();
        File file = new File(path+"\\"+filename);
        if (file.exists()){
            File f = new File(FileMain.initialpath+"\\"+copypath);
            if (f.isDirectory() && f.exists()){
                copyprocess(filename,copypath,path);
            }
            else{
                System.out.println("directory to copy file does not exist");
            }
        }
        else{
            System.out.println("file specified does not exist");
        }
    }
    private static void copyprocess(String filename, String copypath, String path) {
        try {
            File f2 = new File(FileMain.initialpath+"\\"+copypath,filename);
            if(f2.createNewFile()){
                FileInputStream in = new FileInputStream(path+"\\"+filename);
                FileOutputStream out = new FileOutputStream(FileMain.initialpath+"\\"+copypath+"\\"+filename);

                FileChannel source = in.getChannel();
                FileChannel destination = out.getChannel();

                destination.transferFrom(source, 0, source.size());
                in.close();
                out.close();
            }
            else {
                System.out.println("New File cannot be created or exists aldready");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void writefile(String path, String command) throws IOException {
        String filename = command.substring(command.indexOf(">")+1).trim();
        Scanner in = new Scanner(System.in);
        String textarea = "";
        System.out.println("Enter the text to be written");
        System.out.println("Enter 'stop#' to exit");
        while(!textarea.contains("stop#")){
            textarea+="\n";
            textarea += in.nextLine();
        }
        Files.write(Paths.get(path+"\\"+filename), textarea.replace("stop#","").getBytes());
    }
}
